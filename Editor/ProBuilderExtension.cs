
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEditor.ProBuilder;
using UnityEngine;

using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;

namespace SixParQuatre.ProBuilder
{
    public static class ProBuilderExtension
    {
        const string probuilderMenuRoot = "Tools/ProBuilder/Geometry/";
        const string CreateTriangleStr = "Triangulate Selection";


        [InitializeOnLoadMethod]
        public static void OnInit()
        {
        }

        [MenuItem(probuilderMenuRoot + CreateTriangleStr, true)]
        public static bool CreateTriangles_Valid() => HasEnoughVerticesSelected(3);


        public static bool HasEnoughVerticesSelected (int minVertices)
        {
            var selectMode = ProBuilderEditor.selectMode;

            if (selectMode != SelectMode.Vertex)
                return false;

            return MeshSelection.selectedVertexCount >= minVertices;
        }

        [MenuItem(probuilderMenuRoot + CreateTriangleStr)]
        public static void CreateTriangles() => CreateTriangles_WithRes();

        public static ActionResult CreateTriangles_WithRes()
        {
            if (!CreateTriangles_Valid())
                return new ActionResult(ActionResult.Status.NoChange, "Nothing to make triangle out off");

            var selection = MeshSelection.top;
            var selectMode = ProBuilderEditor.selectMode;

            int elemCreated = 0;
            foreach (var mesh in selection)
            {
                switch (selectMode)
                {
                    case SelectMode.Vertex:

                        Undo.RecordObject(mesh, CreateTriangleStr);
                        Transform camTransform = SceneView.lastActiveSceneView.camera.transform;
                        int[] vertIndices = new int[3];
                        for (int i = 0; i <= mesh.selectedVertices.Count - 3; ++i)
                        {
                            vertIndices[0] = mesh.selectedVertices[i];
                            vertIndices[1] = mesh.selectedVertices[i+1];
                            vertIndices[2] = mesh.selectedVertices[i+2];
                            CreateFace(mesh, vertIndices, camTransform);
                            ++elemCreated;
                        }

                        mesh.ToMesh();
                        mesh.Refresh();
                        break;
                }
            }
            ProBuilderEditor.Refresh();

            return new ActionResult(ActionResult.Status.Success, $"{elemCreated} Triangle(s) Created from Selection");
        }

        static void CreateFace(ProBuilderMesh mesh, IList<int> vertIndices, Transform camTransform)
        {
            var face = AppendElements.CreatePolygon(mesh, vertIndices, true);
            Vector3 normal = Math.Normal(mesh, face);
            float angle = Vector3.SignedAngle(camTransform.forward, normal, camTransform.up);
            if (Mathf.Abs(angle) < 90)
                face.Reverse();
        }

        public static void GetIcons(string baseName, out Texture2D enabledIcon, out Texture2D disabledIcon)
        {
            enabledIcon = null;
            disabledIcon = null;
            foreach (string guid in AssetDatabase.FindAssets(baseName))
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guid);
                if(assetPath.Contains("disabled"))
                    disabledIcon = AssetDatabase.LoadAssetAtPath<Texture2D>(assetPath);
                else
                    enabledIcon = AssetDatabase.LoadAssetAtPath<Texture2D>(assetPath);

                if (enabledIcon != null && disabledIcon != null)
                    break;
            }
        }


    }
}
