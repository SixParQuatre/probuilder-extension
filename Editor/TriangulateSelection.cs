using UnityEditor;
using UnityEditor.ProBuilder;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.ProBuilder.MeshOperations;


namespace SixParQuatre.ProBuilder
{
    [ProBuilderMenuAction]
    public class TriangulateSelection: MenuAction
	{
        public override ToolbarGroup group
        {
            get { return ToolbarGroup.Geometry; }
        }


        static Texture2D iconTexture_enabled;
        static Texture2D iconTexture_disabled;
        public override Texture2D icon
        {
            get {
                if (iconTexture_enabled == null)
                    ProBuilderExtension.GetIcons("Vertex_TriangulateSelection", out iconTexture_enabled, out iconTexture_disabled);

                if (iconTexture_disabled != null)
                    return enabled ? iconTexture_enabled : iconTexture_disabled;
                else
                    return iconTexture_enabled;
            }
        }

        public override TooltipContent tooltip
        {
            get { return s_Tooltip; }
        }

        static readonly TooltipContent s_Tooltip = new TooltipContent
            (
                "Triangulate Selection",
                @"Create triangles from the selected vertices"
            );

        public override SelectMode validSelectModes
        {
            get { return SelectMode.Vertex; }
        }

        public override bool enabled
        {
            get { return base.enabled && ProBuilderExtension.CreateTriangles_Valid(); }
        }

        protected override ActionResult PerformActionImplementation() => ProBuilderExtension.CreateTriangles_WithRes();
    }
}