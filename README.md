# ProBuilder Extension for ProBuilder 5.1


ProBuilder is far from perfect (actually if you're not bound to it, you should consider [Realtime CSG](https://github.com/LogicalError/realtime-CSG-for-unity/tree/master) instead) so I created a package to add simple functionalities that I wish were there.
<< [Description](#description) | [Installation](#installation-for-unity-2020-or-later)>>

### What's new? [See changelog!](https://gitlab.com/SixParQuatre/probuilder-extension/-/blob/main/CHANGELOG.md?ref_type=heads) What's next? [See backlog !](https://gitlab.com/SixParQuatre/probuilder-extension/-/issues)



## Features

* ![image](https://gitlab.com/SixParQuatre/probuilder-extension/uploads/bace5551d9bd6575146cd19befd31f32/Vertex_TriangulateSelection.png) **'Tools/ProBuilder/Geometry/Triangulate Selection'**: creates triangle using selected vertices. User can create a shortcut for it in Edit/Shortcuts

>![image](https://gitlab.com/SixParQuatre/probuilder-extension/uploads/3e70a65e4de5f24c9089f7a852580094/image.png)


## Installation

Open Unity Package Manager via **Window/Package Manager**. 

At the top left of the window, click on the **+**  icon and  choose **Add Package from Git URL** and copy paste https://gitlab.com/SixParQuatre/probuilder-extension.git in the input field.
<br>If not already present ProBuilder 5.1 will also be added to the project.