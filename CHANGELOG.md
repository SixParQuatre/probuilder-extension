Description of package changes in reverse chronological order. It is good practice to use a standard format, like [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
# Changelog

## [0.0.3] - 2023-8-6

### Added
Triangulate Selection now properly appears in the Probuilder Editor Tab.

## [0.0.2] - 2023-8-6

### Changed
Lowered min required version of Unity to 2020.3; just so that peeps can install it. It may not work

## [0.0.1] - 2023-8-6

### Changed
Initial commit: added MemuItem **'Tools/ProBuilder/Geometry/Triangulate Selection'**, which creates triangles using selected vertices. User can create a shortcut for it in Edit/Shortcuts

### Known Issues
Undo/Redo works, but the mesh isn't fully refreshed on Redo.